﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models
{
    public class GameData
    {
        public int gameid;
        public int queryport;
        public string gamename;
        public string description;
        public string secretkey;
        public int disabled_services;
        public int backendflags;

    }
}
