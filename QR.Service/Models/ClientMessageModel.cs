﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models
{
    public class ClientMessageModel : BaseMessage
    {
        public ClientMessageModel(IDictionary<string, object> headers, int key, byte[] message) : base(headers, "ClientMessage")
        {
            this.message = message;
            this.key = key;
        }
        public byte[] message { get; set; }
        public int key { get; set; }
    }
}
