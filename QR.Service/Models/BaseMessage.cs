﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace QR.Service.Models
{
    public class BaseMessage
    {
        public BaseMessage(IDictionary<string, object> headers, string type)
        {
            this.type = type;
            if(headers != null)
            {
                protocol_version = ((byte[])headers["X-OpenSpy-Client-Version"])[0];

                client_address = System.Text.ASCIIEncoding.ASCII.GetString((byte[])headers["X-OpenSpy-Client-Address"]);

                server_address = System.Text.ASCIIEncoding.ASCII.GetString((byte[])headers["X-OpenSpy-Server-Address"]);

                if (protocol_version == 2)
                {
                    var instance_key_string = System.Text.ASCIIEncoding.ASCII.GetString((byte[])headers["X-OpenSpy-Instance-Key"]);
                    instance_key = uint.Parse(instance_key_string);
                }
            }

        }
        public string type { get; set; }
        public int protocol_version { get; set; }
        public uint? instance_key { get; set; }
        public string server_address { get; set; }
        public string client_address { get; set; }

        public string GetInstanceKeyName()
        {
            var endpoint = IPEndPoint.Parse(client_address.ToString());

            var instancekey_name = "INSTANCEKEY_" + instance_key.ToString() + "-" + endpoint.Address.ToString() + "-" + endpoint.Port.ToString();
            return instancekey_name;
        }

        public string GetIPMapName()
        {
            var endpoint = IPEndPoint.Parse(client_address.ToString());
            var ipmap_name = "IPMAP_" + endpoint.Address.ToString() + "-" + endpoint.Port.ToString();
            return ipmap_name;
        }
    }
}
