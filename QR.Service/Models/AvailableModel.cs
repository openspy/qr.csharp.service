﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models
{
    public enum EAvailableStatus
    {
        Available = 0,
        Unavailable,
        TemporarilyUnavailable
    }
    public class AvailableModel : BaseMessage
    {
        public AvailableModel(IDictionary<string, object> headers, EAvailableStatus status) : base(headers, "Available")
        {
            this.status = status;
        }
        public EAvailableStatus status { get; set; }
        public string gamename { get; set; }
    }
}
