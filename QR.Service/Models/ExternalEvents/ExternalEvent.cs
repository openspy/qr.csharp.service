﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models.ExternalEvents
{
    public class ExternalEvent
    {
        public string Type { get; set; }
    }
}
