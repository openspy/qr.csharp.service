﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models.ExternalEvents
{
    public class StateChangeEvent : ExternalEvent
    {
        public StateChangeEvent()
        {
            Type = "StateChangeEvent";
        }
        public string ChangeType { get; set; }
        public string ServerKey { get; set; }
    }
}
