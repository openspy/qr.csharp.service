﻿using System;
using System.Net;

namespace QR.Service.Models.ExternalEvents
{
    public class ServerBrowsingDataMessage : ExternalEvent
    {
        public ServerBrowsingDataMessage()
        {
            Type = "ServerBrowsingDataMessage";
        }
        public string SourceAddress { get; set; }
        public string DestinationAddress { get; set; }
        public string Base64Data { get; set; }
    }
}
