﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models
{
    public class ChallengeModel : BaseMessage
    {
        public ChallengeModel(IDictionary<string, object> headers, string challenge) : base(headers, "Challenge")
        {
            this.challenge = challenge;
        }
        public string challenge { get; set; }
    }
}
