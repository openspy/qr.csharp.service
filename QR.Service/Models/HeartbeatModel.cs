﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Models
{
    public class HeartbeatModel : BaseMessage
    {
        public HeartbeatModel() : base(null, "Heartbeat")
        {

        }
        public HeartbeatModel(IDictionary<string, object> headers, HeartbeatModel src) : base(headers, "Heartbeat")
        {
            keys = src.keys;
            player_keys = src.player_keys;
            team_keys = src.team_keys;
        }
        public Dictionary<string, string> keys { get; set; }
        public List<Dictionary<string, string>> player_keys { get; set; }
        public List<Dictionary<string, string>> team_keys { get; set; }
    }
}
