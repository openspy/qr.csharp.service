﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QR.Service.Events;
using QR.Service.Models;
using QR.Service.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QR.Service
{
    public class AMQPListener
    {
        private readonly IConfiguration _config;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IEventHandlerFactory _eventHandlerFactory;
        public AMQPListener(IConfiguration config, ConnectionFactory connectionFactory, IEventHandlerFactory eventHandlerFactory)
        {
            _config = config;
            _connectionFactory = connectionFactory;
            _eventHandlerFactory = eventHandlerFactory;
        }
        public Task Listen(CancellationToken cancelToken)
        {
            var queueName = _config.GetValue<string>("AMQP:QueueName");

            using (IConnection connection = _connectionFactory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(_config.GetValue<string>("AMQP:ExchangeName"), ExchangeType.Topic, true);

                    var headers = new Dictionary<string, object>();

                    channel.QueueDeclare(queueName, true, false, false, headers);


                    channel.QueueBind(queueName, _config.GetValue<string>("AMQP:ExchangeName"), _config.GetValue<string>("AMQP:IncomingRoutingKey"), headers);



                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += async (ch, ea) =>
                    {
                        if (ea.BasicProperties.Headers.ContainsKey("X-OpenSpy-QR-MessageType"))
                        {
                            Console.WriteLine("Got message: {0}", System.Text.ASCIIEncoding.ASCII.GetString((byte[])ea.BasicProperties.Headers["X-OpenSpy-QR-MessageType"]));
                            var type = System.Text.ASCIIEncoding.ASCII.GetString((byte[])ea.BasicProperties.Headers["X-OpenSpy-QR-MessageType"]);
                            var handler = _eventHandlerFactory.GetHandlerByName(type);
                            if(handler != null)
                            {
                                await handler.Handle(ea, channel);
                            }

                        }
                        channel.BasicAck(ea.DeliveryTag, false);
                    };

                    String consumerTag = channel.BasicConsume(queueName, false, consumer);
                    while (!cancelToken.IsCancellationRequested)
                    {
                        System.Threading.Tasks.Task.Delay(1000).Wait();
                    }

                    channel.BasicCancel(consumerTag);
                }
            }
            return Task.CompletedTask;
        }

    }
}
