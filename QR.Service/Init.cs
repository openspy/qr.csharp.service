﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using QR.Service.Events;
using QR.Service.Services;
using RabbitMQ.Client;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service
{
    public static class Init
    {
        public static IConfiguration Configuration;
        public static IServiceCollection InitCommonDi()
        {
            //setup config
            IConfigurationBuilder configBuilder = new ConfigurationBuilder().AddEnvironmentVariables("CUSTOM_").SetBasePath(System.IO.Directory.GetCurrentDirectory());
            configBuilder.AddJsonFile("config.json");
            configBuilder.AddEnvironmentVariables();
            IConfiguration config = configBuilder.Build();

            Configuration = config;

            //setup our DI
            var serviceProvider = new ServiceCollection()
            .AddLogging(c => {
                c.ClearProviders();
                c.AddConfiguration(config.GetSection("Logging"));
                c.AddConsole();
            })
            .AddSingleton<IConfiguration>(c => config)
            .AddTransient<ConnectionFactory>(c => {
                var cfg = c.GetRequiredService<IConfiguration>();
                UriBuilder uriBuilder = new UriBuilder(cfg.GetValue<string>("AMQP:ConnectionString"));
                return new ConnectionFactory
                {
                    Uri = uriBuilder.Uri
                };
            });

            serviceProvider.AddSingleton<AMQPListener>();

            serviceProvider.AddTransient<IGameLookupService, GameLookupService>();
            serviceProvider.AddTransient<IAMQPMessageSender, AMQPMessageSender>();

            serviceProvider.AddTransient<IEventHandlerFactory, EventHandlerFactory>();
            serviceProvider.AddTransient<IHeartbeatService, HeartbeatService>();


            serviceProvider.AddSingleton<IConnectionMultiplexer>(provider => {
                    var cfg = provider.GetRequiredService<IConfiguration>();
                    return ConnectionMultiplexer.Connect(cfg.GetConnectionString("redisCache"));                
                }
            );
            return serviceProvider;
        }
    }
}
