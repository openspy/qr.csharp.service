﻿using Newtonsoft.Json;
using QR.Service.Models;
using QR.Service.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Events
{
    public class AvailableEvent : IEventHandler
    {
        private IGameLookupService _gameLookupService;
        private IAMQPMessageSender _sender;
        public AvailableEvent(IGameLookupService gameLookupService, IAMQPMessageSender sender)
        {
            _gameLookupService = gameLookupService;
            _sender = sender;
        }
        public string GetEventName()
        {
            return "Available";
        }

        public Task Handle(BasicDeliverEventArgs ea, IModel channel)
        {
            //find saved heart beat, activate if challenge valid, send registered response
            var availableJson = System.Text.Encoding.UTF8.GetString(ea.Body);
            var availableRequest = JsonConvert.DeserializeObject<AvailableModel>(availableJson);

            var gameData = _gameLookupService.LookupGameByName(availableRequest.gamename);

            EAvailableStatus status = EAvailableStatus.Unavailable;
            if(gameData != null)
            {
                status = (EAvailableStatus)gameData.disabled_services;
            }

            var message = new AvailableModel(ea.BasicProperties.Headers, status);
            _sender.SendMessage(ea, channel, GetEventName(), message);
            return Task.CompletedTask;
        }
    }
}
