﻿using QR.Service.Models;
using QR.Service.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Events
{
    public class KeepaliveEvent : IEventHandler
    {
        private IAMQPMessageSender _sender;
        private IHeartbeatService _heartbeatService;
        public KeepaliveEvent(IAMQPMessageSender sender, IHeartbeatService heartbeatService)
        {
            _sender = sender;
            _heartbeatService = heartbeatService;
        }
        public string GetEventName()
        {
            return "Keepalive";
        }

        public Task Handle(BasicDeliverEventArgs ea, IModel channel)
        {
            var msg = new BaseMessage(ea.BasicProperties.Headers, "Keepalive");
            _heartbeatService.RefreshTimeout(msg);
            _sender.SendMessage(ea, channel, "Keepalive", msg);
            return Task.CompletedTask;
        }
    }
}
