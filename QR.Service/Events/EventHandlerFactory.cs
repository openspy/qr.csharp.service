﻿using QR.Service.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace QR.Service.Events
{
    public class EventHandlerFactory : IEventHandlerFactory
    {
        public IServiceProvider _provider;
        public EventHandlerFactory(IServiceProvider provider)
        {
            _provider = provider;
        }
        public IEventHandler GetHandlerByName(string name)
        {
            
            switch (name)
            {
                case "Available":
                    return new AvailableEvent(_provider.GetService<IGameLookupService>(), _provider.GetService<IAMQPMessageSender>());
                case "Challenge":
                    return new ChallengeEvent(_provider.GetService<IAMQPMessageSender>(), _provider.GetService<IHeartbeatService>());
                case "Heartbeat":
                    return new HeartbeatEvent(_provider.GetService<IAMQPMessageSender>(), _provider.GetService<IHeartbeatService>());
                case "Keepalive":
                    return new KeepaliveEvent(_provider.GetService<IAMQPMessageSender>(), _provider.GetService<IHeartbeatService>());
                case "ServerBrowsingDataMessage":
                    return new ServerBrowsingDataMessageEvent(_provider.GetService<IAMQPMessageSender>(), _provider.GetService<IHeartbeatService>());
            }
            throw new NotImplementedException();
        }
    }
}
