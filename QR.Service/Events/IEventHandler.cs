﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Events
{
    public interface IEventHandler
    {
        string GetEventName();
        Task Handle(BasicDeliverEventArgs ea, IModel channel);
    }
}
