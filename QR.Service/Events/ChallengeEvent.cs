﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using QR.Service.Models;
using QR.Service.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Events
{
    public class ChallengeEvent : IEventHandler
    {
        private IAMQPMessageSender _sender;
        private IHeartbeatService _heartbeatService;
        public ChallengeEvent(IAMQPMessageSender sender, IHeartbeatService heartbeatService)
        {
            _sender = sender;
            _heartbeatService = heartbeatService;
        }
        public string GetEventName()
        {
            return "Challenge";
        }

        public async Task Handle(BasicDeliverEventArgs ea, IModel channel)
        {
            //find saved heart beat, activate if challenge valid, send registered response
            var challengeJson = System.Text.Encoding.UTF8.GetString(ea.Body);
            var challenge = JsonConvert.DeserializeObject<ChallengeModel>(challengeJson);
            var usableChallenge = new ChallengeModel(ea.BasicProperties.Headers, challenge.challenge);

            var expected_challenge = await  _heartbeatService.GetChallengeResponse(usableChallenge);

            if(usableChallenge.challenge.CompareTo(expected_challenge) == 0)
            {
                await _heartbeatService.SetDeleted(usableChallenge, false);

                //send registered response
                SendServerRegistered(ea, channel);
                
            } else
            {
                //send error response
                Console.WriteLine("got {0} but expected {1}", usableChallenge.challenge, expected_challenge);
            }
        }
        private void SendServerRegistered(BasicDeliverEventArgs ea, IModel channel)
        {
            var message = new BaseMessage(ea.BasicProperties.Headers, "ServerRegistered");
            _sender.SendMessage(ea, channel, "ServerRegistered", message);
        }
    }
}
