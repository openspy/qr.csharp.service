﻿using Newtonsoft.Json;
using QR.Service.Models;
using QR.Service.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Events
{
    public class HeartbeatEvent : IEventHandler
    {
        private IAMQPMessageSender _sender;
        private IHeartbeatService _heartbeatService;
        public HeartbeatEvent(IAMQPMessageSender sender, IHeartbeatService heartbeatService)
        {
            _heartbeatService = heartbeatService;
            _sender = sender;
        }
        public string GetEventName()
        {
            return "Heartbeat";
        }

        public async Task Handle(BasicDeliverEventArgs ea, IModel channel)
        {
            //handle heartbeat
            var heartbeatJson = System.Text.Encoding.UTF8.GetString(ea.Body);
            var decoded_heartbeat = JsonConvert.DeserializeObject<HeartbeatModel>(heartbeatJson);
            var heartbeat = new HeartbeatModel(ea.BasicProperties.Headers, decoded_heartbeat);

            if(!heartbeat.keys.ContainsKey("gamename"))
            {
                return;
            }

            //save server data, and expected challenge
            var generated_challenge = await _heartbeatService.SubmitHeartbeat(heartbeat);


            if(generated_challenge != null)
            {
                //send back challenge
                var challenge = new ChallengeModel(ea.BasicProperties.Headers, generated_challenge);
                _sender.SendMessage(ea, channel, "Challenge", challenge);
            }
        }
    }
}
