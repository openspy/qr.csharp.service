﻿using Newtonsoft.Json;
using QR.Service.Models;
using QR.Service.Models.ExternalEvents;
using QR.Service.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Events
{
    public class ServerBrowsingDataMessageEvent : IEventHandler
    {
        private IAMQPMessageSender _sender;
        private IHeartbeatService _heartbeatService;
        public ServerBrowsingDataMessageEvent(IAMQPMessageSender sender, IHeartbeatService heartbeatService)
        {
            _sender = sender;
            _heartbeatService = heartbeatService;
        }
        public string GetEventName()
        {
            return "ClientMessage";
        }

        public async Task Handle(BasicDeliverEventArgs ea, IModel channel)
        {
            var messageJson = System.Text.Encoding.UTF8.GetString(ea.Body);
            var decoded_message = JsonConvert.DeserializeObject<ServerBrowsingDataMessage>(messageJson);

            var dest_address = System.Net.IPEndPoint.Parse(decoded_message.DestinationAddress);

            //lookup IP & instance key
            var instance_key = await _heartbeatService.GetInstanceKeyFromAddress(dest_address);
            var driver_ip = await _heartbeatService.GetServerAddressFromClientAddress(dest_address);

            if (instance_key == null || driver_ip == null) return;

            //send msg
            var headers = new Dictionary<string, object>();
            headers["X-OpenSpy-QR-MessageType"] = GetEventName();
            headers["X-OpenSpy-Instance-Key"] = System.Text.Encoding.ASCII.GetBytes(instance_key);

            headers["X-OpenSpy-Client-Address"] = System.Text.Encoding.ASCII.GetBytes(decoded_message.DestinationAddress);
            headers["X-OpenSpy-Server-Address"] = System.Text.Encoding.ASCII.GetBytes(driver_ip);

            headers["X-OpenSpy-Client-Version"] = new byte[1] { 2 }; //this only works on QR v2

            headers["X-OpenSpy-AppName"] = "QR.Service";
            headers["X-OpenSpy-Hostname"] = "QR.Service"; //XXX: read config

            var message = new ClientMessageModel(headers, 1111, System.Convert.FromBase64String(decoded_message.Base64Data));
            _sender.SendMessage(ea, channel, GetEventName(), message);
        }
    }
}
