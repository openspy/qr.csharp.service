﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Events
{
    public interface IEventHandlerFactory
    {
        IEventHandler GetHandlerByName(string name);
    }
}
