﻿using QR.Service.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Services
{
    public interface IGameLookupService
    {
        GameData LookupGameByName(string gamename);
    }
}
