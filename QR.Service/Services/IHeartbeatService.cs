﻿using QR.Service.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Services
{
    public interface IHeartbeatService
    {
        Task<string> SubmitHeartbeat(HeartbeatModel model);
        Task<string> GetChallengeResponse(BaseMessage model);
        Task SetChallenge(string server_key, string challenge);
        Task SetDeleted(BaseMessage model, bool deleted);
        Task RefreshTimeout(BaseMessage model);


        Task<string> GetInstanceKeyFromAddress(IPEndPoint address);
        Task<string> GetServerAddressFromClientAddress(IPEndPoint address);
    }
}
