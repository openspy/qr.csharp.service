﻿using Microsoft.Extensions.Configuration;
using QR.Service.Models;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace QR.Service.Services
{
    public class HeartbeatService : IHeartbeatService
    {
        private const int REDIS_SERVER_DB = 0;
        private const int SERVER_EXPIRE_MINS = 5;
        private IConnectionMultiplexer _redisMultiplexer;
        private IGameLookupService _gameLookupService;
        private IAMQPMessageSender _sender;
        private IConfiguration _config;
        public HeartbeatService(IConnectionMultiplexer redisMultiplexer, IGameLookupService gameLookupService, IAMQPMessageSender sender, IConfiguration config)
        {
            _redisMultiplexer = redisMultiplexer;
            _gameLookupService = gameLookupService;
            _sender = sender;
            _config = config;
        }

        public async Task RefreshTimeout(BaseMessage message)
        {
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);

            var server_key = await db.StringGetAsync(message.GetInstanceKeyName().ToString());

            await db.KeyExpireAsync(message.GetInstanceKeyName(), DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
            await db.KeyExpireAsync(message.GetIPMapName(), DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
            await db.KeyExpireAsync(server_key.ToString(), DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
        }

        public async Task SetChallenge(string server_key, string challenge)
        {
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);
            if (challenge == null)
            {
                await db.HashDeleteAsync(server_key, "expected_challenge");
                await SetDeleted(server_key, false);
            } else
            {
                await db.HashSetAsync(server_key, "expected_challenge", challenge.ToString());
                await SetDeleted(server_key, true);
            }
        }

        public async Task SetDeleted(BaseMessage model, bool deleted)
        {
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);
            var server_key = await db.StringGetAsync(model.GetInstanceKeyName());
            await SetDeleted(server_key, deleted);
        }

        private async Task SetDeleted(string server_key, bool deleted)
        {
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);
            await db.HashSetAsync(server_key, "deleted", deleted ? "1" : "0");
        }

        private async Task SetServer(string server_key, HeartbeatModel heartbeat, bool newServer, int id)
        {
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);

            //ip lookup
            var endpoint = IPEndPoint.Parse(heartbeat.client_address.ToString());


            var game_data = _gameLookupService.LookupGameByName(heartbeat.keys["gamename"].ToString());


            await db.HashSetAsync(server_key, "id", id.ToString());
            await db.HashSetAsync(server_key, "gameid", game_data.gameid.ToString());

            await db.HashSetAsync(server_key, "wan_ip", endpoint.Address.ToString());
            await db.HashSetAsync(server_key, "wan_port", endpoint.Port.ToString());
            await db.HashSetAsync(server_key, "instance_key", heartbeat.instance_key.ToString());
            await db.HashSetAsync(server_key, "server_address", heartbeat.server_address.ToString());

            if (heartbeat.keys.ContainsKey("statechanged"))
            {
                var statechanged_value = int.Parse(heartbeat.keys["statechanged"].ToString());
                if (statechanged_value == 2)
                {
                    await SetDeleted(server_key, true);
                    SendGlobalServerEvent(server_key, "Delete");
                } else
                {
                    if(newServer)
                    {
                        SendGlobalServerEvent(server_key, "New");
                    } else
                    {
                        SendGlobalServerEvent(server_key, "Update");
                    }
                    
                }
            }

            await db.StringSetAsync(heartbeat.GetIPMapName(), server_key);

            await db.StringSetAsync(heartbeat.GetInstanceKeyName(), server_key);

            await db.KeyExpireAsync(heartbeat.GetIPMapName(), DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
            await db.KeyExpireAsync(heartbeat.GetInstanceKeyName(), DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
            await db.KeyExpireAsync(server_key, DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));


            //server keys
            var custkey_name = server_key + "custkeys";
            foreach (var item in heartbeat.keys)
            {
                await db.HashSetAsync(custkey_name, item.Key, item.Value);
            }
            await db.KeyExpireAsync(custkey_name, DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));


            //player keys
            var playerkey_name_prefix = server_key + "custkeys_player_";
            for (var i = 0; i < heartbeat.player_keys.Count; i++)
            {
                var playerkey_name = playerkey_name_prefix + i.ToString();
                foreach (var item in heartbeat.player_keys[i])
                {
                    await db.HashSetAsync(playerkey_name, item.Key, item.Value);
                }
                await db.KeyExpireAsync(playerkey_name, DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
            }

            //team keys
            var teamkey_name_prefix = server_key + "custkeys_team_";
            for (var i = 0; i < heartbeat.team_keys.Count; i++)
            {
                var teamkey_name = teamkey_name_prefix + i.ToString();
                foreach (var item in heartbeat.team_keys[i])
                {
                    await db.HashSetAsync(teamkey_name, item.Key, item.Value);
                }
                await db.KeyExpireAsync(teamkey_name, DateTime.Now.AddMinutes(SERVER_EXPIRE_MINS));
            }
        }
        private async Task<Tuple<bool, int>> GenerateID(BaseMessage model)
        {
            var instancekey_name = model.GetInstanceKeyName();
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);
            int id_value;

            if (db.KeyExists(instancekey_name)) //instance key match
            {
                var server_key = db.StringGet(instancekey_name);
                if (
                    (!await db.HashExistsAsync(server_key.ToString(), "deleted") //if deleted key doesn't exist
                    || (await db.HashGetAsync(server_key.ToString(), "deleted")).ToString().CompareTo("1") != 0) //or deleted value is not 1
                    || await db.HashExistsAsync(server_key.ToString(), "expected_challenge")) //and there is no stored challenge
                {
                    var id_string = await db.HashGetAsync(server_key.ToString(), "id");
                    id_value = int.Parse(id_string.ToString());
                    return new Tuple<bool, int>(false, id_value);

                }
            }
            var id = db.StringIncrement("QRID");
            id_value = int.Parse(id.ToString());

            if (await db.KeyExistsAsync(model.GetIPMapName())) //delete old IPMAP server
            {
                var old_serverkey = await db.StringGetAsync(model.GetIPMapName());
                await SetDeleted(old_serverkey.ToString(), true);
            }

            return new Tuple<bool, int>(true, id_value);
        }
        public async Task<string> SubmitHeartbeat(HeartbeatModel model)
        {
            var id_result = await GenerateID(model);

            var game = _gameLookupService.LookupGameByName(model.keys["gamename"]);


            var server_key = game.gamename + ":" + id_result.Item2 + ":";

            //save server data
            await SetServer(server_key, model, id_result.Item1, id_result.Item2);

            //generate challenge
            var generated_challenge = "x9DzD9p20rmUQBUqmiB6";

            //calculate expected challenge
            if (id_result.Item1)
            {
                var inputBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(generated_challenge);
                var result = RC4.Encrypt(System.Text.ASCIIEncoding.ASCII.GetBytes(game.secretkey), inputBytes);

                var expected_challenge = GsEncode.Encode(result);
                await SetChallenge(server_key, expected_challenge);
                return generated_challenge;
            }

            return null;
        }

        public async Task<string> GetChallengeResponse(BaseMessage message)
        {
            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);
            if (db.KeyExists(message.GetInstanceKeyName()))
            {
                var server_key = await db.StringGetAsync(message.GetInstanceKeyName());

                if (await db.HashExistsAsync(server_key.ToString(), "expected_challenge"))
                {
                    var challenge = await db.HashGetAsync(server_key.ToString(), "expected_challenge");

                    return challenge.ToString();
                }
            }
            return null;
        }

        private void SendGlobalServerEvent(string server_key, string event_name)
        {
            var eventModel = new Models.ExternalEvents.StateChangeEvent { ChangeType = event_name, ServerKey = server_key };
            _sender.SendMessage(_config.GetValue<string>("AMQP:GlobalRoutingKey"), "ServerEvent", eventModel);
        }

        public async Task<string> GetInstanceKeyFromAddress(IPEndPoint address)
        {
            var ipmap_model = new BaseMessage(null, "");
            ipmap_model.client_address = address.ToString();
            var ipmap_name = ipmap_model.GetIPMapName();

            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);

            if(db.KeyExists(ipmap_name))
            {
                var server_key = await db.StringGetAsync(ipmap_name);

                return await db.HashGetAsync(server_key.ToString(), "instance_key");
            }

            return null;
        }

        public async Task<string> GetServerAddressFromClientAddress(IPEndPoint address)
        {
            var ipmap_model = new BaseMessage(null, "");
            ipmap_model.client_address = address.ToString();
            var ipmap_name = ipmap_model.GetIPMapName();

            var db = _redisMultiplexer.GetDatabase(REDIS_SERVER_DB);

            if (db.KeyExists(ipmap_name))
            {
                var server_key = await db.StringGetAsync(ipmap_name);

                return await db.HashGetAsync(server_key.ToString(), "server_address");
            }

            return null;
        }
    }
}
