﻿using QR.Service.Models;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Services
{
    public class GameLookupService : IGameLookupService
    {
        private const int REDIS_GAME_DB = 2;
        private IConnectionMultiplexer redisMultiplexer;
        public GameLookupService(IConnectionMultiplexer redisMultiplexer)
        {
            this.redisMultiplexer = redisMultiplexer;
        }
        private IDatabase GetDatabase()
        {
            return redisMultiplexer.GetDatabase(REDIS_GAME_DB);
        }
        public GameData LookupGameByName(string gamename)
        {
            var db = GetDatabase();
            var key = db.StringGet(gamename);
            if (!key.HasValue) return null;
            return LookupGameByGameKey(db, key.ToString());
        }
        private GameData LookupGameByGameKey(IDatabase database, string key)
        {
            var result = new GameData();
            database.HashGet(key, "gameid").TryParse(out result.gameid);
            database.HashGet(key, "queryport").TryParse(out result.queryport);
            result.gamename = database.HashGet(key, "gamename").ToString();
            result.description = database.HashGet(key, "description").ToString();
            result.secretkey = database.HashGet(key, "secretkey").ToString();
            database.HashGet(key, "disabledservices").TryParse(out result.disabled_services);
            database.HashGet(key, "backendflags").TryParse(out result.backendflags);
            return result;
        }
    }
}
