﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Services
{
    public class AMQPMessageSender : IAMQPMessageSender
    {
        private readonly IConfiguration _config;
        private ConnectionFactory _connectionFactory;
        public AMQPMessageSender(IConfiguration config, ConnectionFactory connectionFactory)
        {
            _config = config;
            _connectionFactory = connectionFactory;
        }
        public void SendMessage(BasicDeliverEventArgs ea, IModel channel, string type, object inputObject)
        {
            IBasicProperties props = channel.CreateBasicProperties();
            props.ContentType = "text/plain";
            props.DeliveryMode = 2;
            props.Headers = SetupHeaders(type, ea.BasicProperties.Headers);

            string routing_key = _config.GetValue<string>("AMQP:OutgoingRoutingKey");

            if (ea.BasicProperties.Headers.ContainsKey("X-OpenSpy-ReplyTo"))
            {
                routing_key = System.Text.ASCIIEncoding.ASCII.GetString((byte[])ea.BasicProperties.Headers["X-OpenSpy-ReplyTo"]);
            }

            var json = JsonConvert.SerializeObject(inputObject);
            channel.BasicPublish(_config.GetValue<string>("AMQP:ExchangeName"), routing_key, props, System.Text.ASCIIEncoding.ASCII.GetBytes(json));
        }
        static IDictionary<string, object> SetupHeaders(string messageType, IDictionary<string, object> originalHeaders)
        {
            var Headers = new Dictionary<string, object>(originalHeaders);
            Headers["X-OpenSpy-QR-MessageType"] = messageType;
            /*Headers["X-OpenSpy-AppName"] = originalHeaders["X-OpenSpy-AppName"];
            Headers["X-OpenSpy-Hostname"] = originalHeaders["X-OpenSpy-Hostname"];

            Headers["X-OpenSpy-Client-Version"] = originalHeaders["X-OpenSpy-Client-Version"];
            Headers["X-OpenSpy-Client-Address"] = originalHeaders["X-OpenSpy-Client-Address"];
            Headers["X-OpenSpy-Instance-Key"] = originalHeaders["X-OpenSpy-Instance-Key"];*/
            return Headers;
        }
        public void SendMessage(string routing_key, string type, object inputObject)
        {
            using (IConnection connection = _connectionFactory.CreateConnection())
            {
                using (IModel channel = connection.CreateModel())
                {

                    IBasicProperties props = channel.CreateBasicProperties();
                    props.ContentType = "text/plain";
                    props.DeliveryMode = 2;

                    channel.ExchangeDeclare(_config.GetValue<string>("AMQP:ExchangeName"), ExchangeType.Topic, true);

                    var json = JsonConvert.SerializeObject(inputObject);
                    channel.BasicPublish(_config.GetValue<string>("AMQP:ExchangeName"), routing_key, props, System.Text.ASCIIEncoding.ASCII.GetBytes(json));
                }
            }
        }
    }
}
