﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service.Services
{
    public interface IAMQPMessageSender
    {
        void SendMessage(BasicDeliverEventArgs ea, IModel channel, string type, object json);
        void SendMessage(string routing_key, string type, object inputObject);
    }
}
