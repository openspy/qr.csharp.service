﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace QR.Service
{
    class Program
    {
        static void Main(string[] args)
        {

            var di = Init.InitCommonDi();
            var serviceProvider = di.BuildServiceProvider();
            using (var tokenSource = new System.Threading.CancellationTokenSource())
            {
                var token = tokenSource.Token;
                var service = serviceProvider.GetRequiredService<AMQPListener>();
                var task = service.Listen(token);
                task.Wait();
            }
        }
    }
}
