﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QR.Service
{
    public static class GsEncode
    {
		private static char encode_ct(char c)
		{
			if (c < 26) return (char)('A' + c);
			if (c < 52) return (char)('a' + c - 26);
			if (c < 62) return (char)('0' + c - 52);
			if (c == 62) return (char)('+');
			if (c == 63) return (char)('/');

			return (char)0;
		}
		public static string Encode(byte[] input)
		{
			string output = "";
			int i, pos;
			byte[] trip = new byte[3];
			byte[] kwart = new byte[4];
			i = 0;
			int size = input.Length;

			var readIndex = 0;
			while (i < size)
			{
				for (pos = 0; pos <= 2; pos++, i++)
					if (i < size)
					{

						trip[pos] = input[readIndex];
						readIndex++;
					}
					else trip[pos] = 0;
				kwart[0] = (byte)((trip[0]) >> 2);
				kwart[1] = (byte)((((trip[0]) & 3) << 4) + ((trip[1]) >> 4));
				kwart[2] = (byte)((((trip[1]) & 15) << 2) + ((trip[2]) >> 6));
				kwart[3] = (byte)((trip[2]) & 63);
				for (pos = 0; pos <= 3; pos++) output += encode_ct((char)kwart[pos]);
			}
			return output;
		}
	}
}
